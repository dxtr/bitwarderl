-module(device).

-export([regenerate_tokens/1,
	 regenerate_tokens/2,
	 regenerate_tokens/3,
	 find_by_refresh_token/1]).

-record(device, {
	  id,
	  uuid,
	  inserted_at,
	  updated_at,
	  user_id,
	  name,
	  type,
	  push_token,
	  access_token,
	  refresh_token,
	  token_expires_at
}).

device_from_query(Columns, Rows) ->
    lists:map(fun ({{column, Name, _, _, _, _}, Data}) -> {Name,Data} end, lists:zip(Columns, tuple_to_list(Data))).

regenerate_tokens(Device) ->
    regenerate_tokens(Device, 60*6).

regenerate_tokens(Device, Validity) ->
    Token = base64url:encode(crypto:strong_rand_bytes(64)),
    regenerate_tokens(Device, Token, Validity).

regenerate_tokens(Device, Token, Validity) ->
    ok.

find_by_id(ID) ->
    {ok, Columns, Rows} = pgapp:equery("SELECT * FROM devices WHERE id = $1", [ID]).

find_by_refresh_token(Token) ->
    ok.
