-module(cipherstring).

-export([to_string/1,
	 from_string/1]).
-export_type([cipherstring_type/0]).

-include("cipherstring.hrl").

-define(CIPHER_STRING_TYPE_ENUM,
	[{aes_cbc256_b64, <<"0">>},
	 {aes_cbc128_hmac_sha256_b64, <<"1">>},
	 {aes_cbc256_hmac_sha256_b64, <<"2">>},
	 {rsa2048_oaep_sha256_b64, <<"3">>},
	 {rsa2048_oaep_sha1_b64, <<"4">>},
	 {rsa2048_oaep_sha256_hmac_sha256_b64, <<"5">>},
	 {rsa2048_oaep_sha1_hmac_sha256_b64, <<"6">>}]).

-define(CIPHER_STRING_TYPE_REVERSE_ENUM,
	lists:map(fun ({K,V}) ->
			  {V,K}
		  end, ?CIPHER_STRING_TYPE_ENUM)).

-spec type_to_string(cipherstring_type()) -> binary() | undefined.
type_to_string(Type) ->
    proplists:get_value(Type, ?CIPHER_STRING_TYPE_ENUM, undefined).

-spec type_from_string(binary()) -> cipherstring_type() | undefined.
type_from_string(String) ->
    proplists:get_value(String, ?CIPHER_STRING_TYPE_REVERSE_ENUM, undefined).

-spec from_string(binary()) -> {ok, #cipherstring{}}
				   | {error, binary() | invalid_cipherstring}.
from_string(Str) ->
    Options = [notempty,
	       bsr_unicode,
	       {capture, ['TYPE', 'IV', 'CTMAC'], binary}],
    Regex = <<"^(?<TYPE>\\d)\\.(?<IV>[^|]+?)\\|(?<CTMAC>.+)$">>,
    case re:run(Str, Regex, Options) of
	nomatch -> {error, invalid_cipherstring};
	{error, {compile, CompileErr}} ->
	    {error, CompileErr};
	{match, [Type, Iv, Ctmac]} ->
	    {Ct, Mac} = case binary:split(Ctmac, <<"|">>) of
			    [C,M] -> {C,M};
			    [C] -> {C, undefined}
			end,
	    {ok, #cipherstring{
		    type = type_from_string(Type),
		    iv = Iv,
		    ct = Ct,
		    mac = Mac
		   }}
    end.

-spec to_string(#cipherstring{}) -> binary().
to_string(#cipherstring{type=Type,
			iv=Iv,
			ct=Ct,
			mac=Mac}) ->
    Concat = fun(L) -> [H|T] = lists:reverse(L),
			lists:foldl(
			  fun(X,Acc) ->
				  <<X/binary,"|",Acc/binary>>
			  end, H, T)
	     end,
    Filter = fun(L) -> lists:filter(fun(<<>>) -> false;
				       (_) -> true
				    end, L)
	     end,

    TypeStr = type_to_string(Type),
    StrList = case Mac of
		  undefined -> [<<TypeStr/binary, ".", Iv/binary>>,
				<<Ct/binary>>];
		  <<>> -> [<<TypeStr/binary, ".", Iv/binary>>,
			   <<Ct/binary>>];
		  _ -> [<<TypeStr/binary, ".", Iv/binary>>,
			    <<Ct/binary, "|", Mac/binary>>]
	      end,
    Concat(Filter(StrList)).

