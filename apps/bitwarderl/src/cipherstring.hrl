-record(cipherstring, {type, iv, ct, mac}).
-type cipherstring_type() :: aes_cbc256_b64
			   | aes_cbc128_hmac_sha256_b64
			   | aes_cbc256_hmac_sha256_b64
			   | rsa2048_oaep_sha256_b64
			   | rsa2048_oaep_sha1_b64
			   | rsa2048_oaep_sha256_hmac_sha256_b64
			   | rsa2048_oaep_sha1_hmac_sha256_b64.
