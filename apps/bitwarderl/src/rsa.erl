-module(rsa).

-include_lib("public_key/include/public_key.hrl").

-export([generate_key/0,
	 generate_key/1,
	 sign/2]).

generate_key() ->
    generate_key(8192).

generate_key(Size) ->
    public_key:generate({rsa, Size, 65537}).

sign(Msg, Key) ->
    public_key:sign(Msg, sha256, Key).


