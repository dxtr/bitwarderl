%%%-------------------------------------------------------------------
%% @doc bitwarderl public API
%% @end
%%%-------------------------------------------------------------------

-module(bitwarderl_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    lager:start(),
    application:ensure_all_started(pgapp),
    pgapp:connect([{size, 5}, {database, "bitwardelx"}, {username, "bitwardelx"}, {password, "devdatabase"}]),
    Paths = [{"/", hello_handler, []},
             {"/api/accounts/register", accounts_register_handler, []},
             {"/identity/connect/token", identity_connect_token_handler, []}],
    Dispatch = cowboy_router:compile([
                                      {'_', Paths}]),
    {ok, _} = cowboy:start_clear(my_http_listener,
                                 [{port, 8080}],
                                 #{env => #{dispatch => Dispatch}}),
    %% {ok, _} = cowboy:start_tls(my_https_listener,
    %%                            [{port, 8443},
    %%                             {certfile, ""},
    %%                             {keyfile, ""}],
    %%                           #{env => #{dispatch => Dispatch}}),
    
    

    bitwarderl_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
