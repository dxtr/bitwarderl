-module(identity_connect_token_handler).

-export([init/2, terminate/3]).

init(Req=#{method := <<"POST">>}, State) ->
    {ok, RequestData, _} = cowboy_req:read_urlencoded_body(Req),
    lager:log(info, self(), "RequestData: ~p", [RequestData]),
    ResponseData = #{
      <<"access_token">> => <<"">>,
      <<"expires_in">> => 3600,
      <<"token_type">> => <<"">>,
      <<"refresh_token">> => <<"">>,
      <<"Key">> => <<"">>
     },
    Resp = cowboy_req:reply(200,
			    #{<<"content-type">> => <<"application/json">>},
			    jsone:encode(ResponseData),
			    Req),  
    {ok, Resp, State};
init(Req, State) ->
    Resp = cowboy_req:reply(405, #{
			      <<"allow">> => <<"GET">>
			     }, Req),
    {ok, Resp, State}.

terminate(_Reason, _Req, _State) ->
    ok.
