-module(bitwarden).

-include("cipherstring.hrl").

-export([make_key/3,
	 make_enc_key/1,
	 make_enc_key/3,
	 hash_password/2,
	 encrypt/4,
	 encrypt/5,
	 decrypt/3,
	 macs_equal/3,
	 pad/2,
	 unpad/1,
	 hash_length/1,
	 iv_length/1,
	 key_length/1,
	 block_size/1]).

hash_length(sha) -> 20;
hash_length(sha224) -> 28;
hash_length(sha256) -> 32;
hash_length(sha384) -> 48;
hash_length(sha512) -> 64.

iv_length(aes_cbc) -> block_size(aes_cbc);
iv_length(aes_cbc128) -> block_size(aes_cbc128);
iv_length(aes_cbc256) -> block_size(aes_cbc256).

key_length(aes_cbc) -> 16;
key_length(aes_cbc128) -> 16;
key_length(aes_cbc256) -> 32.

block_size(aes_cbc) -> 16;
block_size(aes_cbc128) -> 16;
block_size(aes_cbc256) -> 16.

%%-spec pad(binary(),integer()) -> binary().
pad(Cipher, Data) ->
    Blocksize = block_size(Cipher),
    N = Blocksize - (byte_size(Data) rem Blocksize),
    Pad = list_to_binary(lists:duplicate(N,N)),
    <<Data/binary, Pad/binary>>.

unpad(Data) ->
    N = binary:last(Data),
    binary:part(Data, 0, byte_size(Data) - N).

-spec make_key(integer(), binary(), binary()) -> binary().
make_key(Size, Password, Salt) ->
    {ok, Output} = pbkdf2:pbkdf2(sha256, Password, Salt, 5000, Size),
    Output.

-spec make_enc_key(binary()) -> #cipherstring{}.
make_enc_key(Key) ->
    Cipher = aes_cbc256,
    Plaintext = crypto:strong_rand_bytes(block_size(Cipher)),
    IV = crypto:strong_rand_bytes(iv_length(Cipher)),
    make_enc_key(Key, IV, Plaintext).

-spec make_enc_key(binary(), binary(), binary()) -> #cipherstring{}.
make_enc_key(Key, IV, Plaintext) ->
    Cipher = aes_cbc256,
    Ciphertext = crypto:block_encrypt(Cipher, Key, IV, pad(Cipher, Plaintext)),
    #cipherstring {
       type = aes_cbc_256_b64,
       iv = base64:encode(IV),
       ct = base64:encode(Ciphertext)
      }.


-spec hash_password(binary(), binary()) -> binary().
hash_password(Password, Salt) ->
    Key = make_key(256, Password, Salt),
    {ok, Hash} = pbkdf2:pbkdf2(sha256, Key, Salt, 1, 256),
    base64:encode(Hash).

-spec encrypt(atom(), binary(), binary(), binary()) -> #cipherstring{}.
encrypt(Cipher, Key, Mackey, Plaintext) ->
    IV = crypto:strong_rand_bytes(iv_length(Cipher)),
    encrypt(Cipher, IV, Plaintext, Key, Mackey).


%% TODO: Handle encryption of multiple blocks here
-spec encrypt(atom(), binary(), binary(), binary(), binary()) -> #cipherstring{}.
encrypt(Cipher, Key, Mackey, IV, Plaintext) ->
    Ciphertext = crypto:block_encrypt(Cipher, Key, IV, pad(Cipher, Plaintext)),
    file:write_file("test.bin", Ciphertext),
    Mac = crypto:hmac(sha256, Mackey, <<IV/binary, Ciphertext/binary>>),
    #cipherstring{
       type = aes_cbc256_hmac_sha256_b64,
       iv = base64:encode(IV),
       ct = base64:encode(Ciphertext),
       mac = base64:encode(Mac)
      }.

-spec decrypt(#cipherstring{}, binary(), binary()) -> binary()
							  | mac_error
							  | not_implemented.
decrypt(#cipherstring{type=Type,iv=B64IV,ct=B64Ciphertext,mac=B64Mac},
       Key, Mackey) ->
    IV = base64:decode(B64IV),
    Ciphertext = base64:decode(B64Ciphertext),
    Mac = case B64Mac of
	      undefined -> <<"">>;
	      _ -> base64:decode(B64Mac)
	  end,

    case Type of
	aes_cbc256_b64 ->
	    unpad(crypto:block_decrypt(aes_cbc256, Key, IV, Ciphertext));
	aes_cbc256_hmac_sha256_b64 ->
	    Cmac = crypto:hmac(sha256, Mackey, <<IV/binary, Ciphertext/binary>>),
	    case macs_equal(Mackey, Mac, Cmac) of
		true ->
		    unpad(crypto:block_decrypt(aes_cbc256, Key, IV, Ciphertext));
		false ->
		    mac_error
	    end;
	_ -> not_implemented
    end.

-spec macs_equal(binary(), binary(), binary()) -> boolean().
macs_equal(Mackey, Mac1, Mac2) ->
    Hmac1 = crypto:hmac(sha256, Mackey, Mac1),
    Hmac2 = crypto:hmac(sha256, Mackey, Mac2),
    Hmac1 =:= Hmac2.

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

make_enc_key_test() ->
    ?assertMatch(#cipherstring{
		   type = aes_cbc_256_b64,
		   iv = <<"AgICAgICAgICAgICAgICAg==">>,
		   ct = <<"OJFQejJTem+neT+zi3poKw==">>,
		   mac = undefined},
		bitwarden:make_enc_key(binary:copy(<<1>>, 32), binary:copy(<<2>>, 16), <<"foo">>)).

-endif. % TEST
