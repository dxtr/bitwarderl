-module(headers).

-export([header/2,
         content_type/1,
         plaintext_header/0,
         json_header/0]).

header(<<Header>>, <<Value>>) ->
    [{Header, Value}].

content_type(<<Value>>) ->
    header(<<"content-type">>, Value).

plaintext_header() ->
    content_type(<<"text/plain; charset=utf-8">>).

json_header() ->
    content_type(<<"application/json">>).
