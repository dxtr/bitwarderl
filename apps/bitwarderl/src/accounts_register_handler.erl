-module(accounts_register_handler).

%% Request looks like:
%% POST $baseURL/accounts/register
%% Content-type: application/json

%% {
%% 	"name": null,
%% 	"email": "nobody@example.com",
%% 	"masterPasswordHash": "r5CFRR+n9NQI8a525FY+0BPR0HGOjVJX0cR1KEMnIOo=",
%% 	"masterPasswordHint": null,
%% 	"key": "0.uRcMe+Mc2nmOet4yWx9BwA==|PGQhpYUlTUq/vBEDj1KOHVMlTIH1eecMl0j80+Zu0VRVfFa7X/MWKdVM6OM/NfSZicFEwaLWqpyBlOrBXhR+trkX/dPRnfwJD2B93hnLNGQ=",
%% }

-export([init/2,
	 content_types_accepted/2,
	 allowed_methods/2,
	 from_json/2]).

init(Req, State) ->
    {cowboy_rest, Req, State}.

content_types_accepted(Req, State) ->
    {[
      {{<<"application">>, <<"json">>, '*'}, from_json}
     ], Req, State}.

allowed_methods(Req, State) ->
    {[<<"POST">>], Req, State}.

from_json(Req, State) ->
    cowboy_req:has_body(Req) andalso
	begin
	    {ok, JsonData, _} = cowboy_req:read_body(Req),
	    #{<<"email">> := Email,
	      <<"key">> := Key,
	      <<"masterPasswordHash">> := PasswordHash,
	      <<"masterPasswordHint">> := PasswordHint} = jsone:decode(JsonData),
	    Uuid = uuid:to_string(uuid:uuid4()),
	    %% TODO: Error checking
	    lager:log(info, self(), "~p ~p ~p ~p ~p", [Email, Key,
						       PasswordHash, PasswordHint, Uuid]),
	    {ok, _} = pgapp:equery("insert into accounts (uuid,email,key,password_hash,password_hint) values ($1,$2,$3,$4,$5)",
				   [Uuid, Email, Key, PasswordHash, PasswordHint]),
	    cowboy_req:reply(200, #{}, <<"">>, Req)
	end,
    {stop, Req, State}.
    
	    
