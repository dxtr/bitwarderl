-module(bitwarden_test).

-include_lib("eunit/include/eunit.hrl").

-include("src/cipherstring.hrl").

macs_equal_test() ->
    ?assert(bitwarden:macs_equal(<<"foo">>,
				 <<"bar">>,
				 <<"bar">>)).
pad_test() ->
    ?assertMatch(<<"foo\r\r\r\r\r\r\r\r\r\r\r\r\r">>,
		bitwarden:pad(aes_cbc256, <<"foo">>)),
    ?assertMatch(<<102,111,111,102,111,111,102,111,
		   111,102,111,111,102,111,111,111,
		   016,016,016,016,016,016,016,016,
		   016,016,016,016,016,016,016,016>>,
		bitwarden:pad(aes_cbc256, <<"foofoofoofoofooo">>)).

encrypt_test() ->
    ?assertMatch(#cipherstring{
		    type = aes_cbc256_hmac_sha256_b64,
		    iv = <<"BAQEBAQEBAQEBAQEBAQEBA==">>,
		    ct = <<"Cy5LE0h2asmJ+WlY2LGJzw==">>,
		    mac = <<"40KQlixRy2NAjRkLBny5GQKYSLtyH6Gay1+3aEhv5yU=">>
		   },
		 bitwarden:encrypt(aes_cbc256,
				   binary:copy(<<2>>, 32),
				   binary:copy(<<3>>, 32),
				   binary:copy(<<4>>, 16),
				   binary:copy(<<"*">>, 1))),
    ?assertMatch(#cipherstring{
		   type = aes_cbc256_hmac_sha256_b64,
		   iv = <<"MTIzNDU2Nzg4NzY1NDMyMQ==">>,
		   ct = <<"t9IU6qwRe5HMI1ehALmg+4PtqtxEvSZk//uSqe5qqts4xDKUuv5f7mY8A8QWd0il">>,
		   mac = <<"8GhII4WEYHw80oRVcEADCfwwI1cOn7Ldcm1MBs/3jHo=">>},
		 bitwarden:encrypt(aes_cbc256,
				   <<"12345678123456781234567812345678">>,
				   <<"87654321876543218765432187654321">>,
				   <<"1234567887654321">>,
				   binary:copy(<<"*">>, 32))).
 
decrypt_test() ->
    ?assertMatch(<<"********************************">>,
		 bitwarden:decrypt(#cipherstring{
				      type = aes_cbc256_hmac_sha256_b64,
				      iv = <<"MTIzNDU2Nzg4NzY1NDMyMQ==">>,
				      ct = <<"t9IU6qwRe5HMI1ehALmg+4PtqtxEvSZk//uSqe5qqts4xDKUuv5f7mY8A8QWd0il">>,
				      mac =
    <<"8GhII4WEYHw80oRVcEADCfwwI1cOn7Ldcm1MBs/3jHo=">>
				     },
				   <<"12345678123456781234567812345678">>,
				   <<"87654321876543218765432187654321">>)),
    ?assertMatch(<<"*">>,
		 bitwarden:decrypt(#cipherstring{
				      type = aes_cbc256_hmac_sha256_b64,
				      iv = <<"BAQEBAQEBAQEBAQEBAQEBA==">>,
				      ct = <<"Cy5LE0h2asmJ+WlY2LGJzw==">>,
				      mac = <<"40KQlixRy2NAjRkLBny5GQKYSLtyH6Gay1+3aEhv5yU=">>
				     },
				   binary:copy(<<2>>, 32),
				   binary:copy(<<3>>, 32)
				  )).
