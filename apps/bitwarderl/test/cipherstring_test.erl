-module(cipherstring_test).

-include_lib("eunit/include/eunit.hrl").

-include("src/cipherstring.hrl").

-define(CIPHERSTRING_REC,
	#cipherstring{
	   type = aes_cbc256_hmac_sha256_b64,
	   iv = <<"Zm9v">>,
	   ct = <<"fSvvDWnOGzhcX9lvgd0/eJAJv7GX46PWjANfaHpwWdg=">>,
	   mac = <<"kqQHwuHJP58GT36D/IxzVksIxSO5jc3jwUBSIbsIw0w=">>
	  }).
-define(CIPHERSTRING_REC2,
	#cipherstring{
	   type = aes_cbc256_hmac_sha256_b64,
	   iv = <<"Zm9v">>,
	   ct = <<"fSvvDWnOGzhcX9lvgd0/eJAJv7GX46PWjANfaHpwWdg=">>,
	   mac = undefined
	  }).
-define(CIPHERSTRING_REC3,
	#cipherstring{
	   type = aes_cbc256_b64,
	   iv = <<"uRcMe+Mc2nmOet4yWx9BwA==">>,
	   ct = <<"PGQhpYUlTUq/vBEDj1KOHVMlTIH1eecMl0j80+Zu0VRVfFa7X/MWKdVM6OM/NfSZicFEwaLWqpyBlOrBXhR+trkX/dPRnfwJD2B93hnLNGQ=">>,
	   mac = undefined
}).

-define(CIPHERSTRING_STR,
	<<"2.Zm9v|fSvvDWnOGzhcX9lvgd0/eJAJv7GX46PWjANfaHpwWdg=|kqQHwuHJP58GT36D/IxzVksIxSO5jc3jwUBSIbsIw0w=">>).
-define(CIPHERSTRING_STR2,
	<<"2.Zm9v|fSvvDWnOGzhcX9lvgd0/eJAJv7GX46PWjANfaHpwWdg=">>).
-define(CIPHERSTRING_STR3,
	<<"0.uRcMe+Mc2nmOet4yWx9BwA==|PGQhpYUlTUq/vBEDj1KOHVMlTIH1eecMl0j80+Zu0VRVfFa7X/MWKdVM6OM/NfSZicFEwaLWqpyBlOrBXhR+trkX/dPRnfwJD2B93hnLNGQ=">>).

to_string_test() ->
    ?assertMatch(?CIPHERSTRING_STR, cipherstring:to_string(?CIPHERSTRING_REC)),
    ?assertMatch(?CIPHERSTRING_STR2, cipherstring:to_string(?CIPHERSTRING_REC2)),
    ?assertMatch(?CIPHERSTRING_STR3, cipherstring:to_string(?CIPHERSTRING_REC3)).

from_string_test() ->
    ?assertMatch({ok, ?CIPHERSTRING_REC}, cipherstring:from_string(?CIPHERSTRING_STR)),
    ?assertMatch({ok, ?CIPHERSTRING_REC2}, cipherstring:from_string(?CIPHERSTRING_STR2)),
    ?assertMatch({ok, ?CIPHERSTRING_REC3}, cipherstring:from_string(?CIPHERSTRING_STR3)).
